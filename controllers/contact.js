/**
 * GET /
 * Contact page.
 */
 const links = require('../config/links');

 exports.index = (req, res) => {
   const active = Object.assign({}, links.menu);
   active.contact = "active";
   res.render('main/contact', {
     active: active,
     title: "Captain design: Contact page to make you the best web design for your company",
     description: "Contact page for website design based on bootstrap",
     keywords: "website, design, webdesign, bootstrap css, landingpage, company website, free website design, free website theme"
   });
 };
