/**
 * GET /
 * Themes page.
 */
const links = require('../config/links');
const themes = require('../config/themes.json')["themes"];

exports.index = (req, res) => {
  const active = Object.assign({}, links.menu);
  active.themes = "active";
  res.render('main/themes', {
    active: active,
    themes: themes,
    title: "Captain design: Free bootstrap themes for your company to download",
    description: "Free bootstrap themes for your company to download",
    keywords: "website, design, webdesign, bootstrap css, landingpage, company website, free website design, free website theme"
  });
};

exports.single = (req, res) => {
  const active = Object.assign({}, links.menu);
  active.themes = "active";
  const theme = themes[req.params.name];

  if (theme === undefined) {
      res.status(err.status || 404);
      res.render('error', {message: "theme does not exist"});
  }
  res.render('main/theme', {
    theme: theme,
    active: active,
    title: "Captain design: Free bootstrap themes for your company to download",
    description: "Download this free website theme done with passion by captain-design.com",
    keywords: "website, design, webdesign, bootstrap css, landingpage, company website, free website design, free website theme"
  });
};
