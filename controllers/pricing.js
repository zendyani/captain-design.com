/**
 * GET /
 * Pricing page.
 */
const links = require('../config/links');

exports.index = (req, res) => {
  const active = Object.assign({}, links.menu);
  active.pricing = "active";
  res.render('pricing', {
    layout: false,
    active: active,
    title: "Captain design: Pricing page to make you the webdesign that your business deserve",
    description: "Be assured to have the best quality at the best price, Money back guarantee in case of dissatisfaction.",
    keywords: "website, design, webdesign, bootstrap css, landingpage, company website, free website design, free website theme"
  });
};
