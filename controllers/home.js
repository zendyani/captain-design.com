/**
 * GET /
 * Home page.
 */
const links = require('../config/links');
const themes = require('../config/themes.json')["themes"];

exports.index = (req, res) => {
  const active = Object.assign({}, links.menu);
  active.home = "active";
  res.render('home', {
    layout: false,
    active: active,
    themes: themes,
    title: "Captain design: Let us make the best webdesign for your business at the best price Or download one for free",
    description: "Make the best website design for your company at the best price, Money back guarantee in case of dissatisfaction, we have free themes for the rest",
    keywords: "website, design, webdesign, bootstrap css, landingpage, company website, free website design, free website theme"
  });
};
