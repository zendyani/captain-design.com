const express = require('express');
const path = require('path');
const compression = require('compression');
const favicon = require('serve-favicon');
const logger = require('morgan');
// const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const helmet = require('helmet');
const expressHbs = require('express-handlebars');
const expressStatusMonitor = require('express-status-monitor');

/**
 * Controllers (route handlers).
 */
const homeController = require('./controllers/home');
const contactController = require('./controllers/contact');
const pricingController = require('./controllers/pricing');
const themesController = require('./controllers/themes');

const app = express();

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.engine('.hbs', expressHbs({ defaultLayout: 'layout', extname: '.hbs' }));
app.set('view engine', 'hbs');
app.use(expressStatusMonitor());
app.use(compression());
app.use(helmet());
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
// app.use('/ui-kit', express.static(path.join(__dirname, 'ui-kit')));
// app.use('/build', express.static(path.join(__dirname, 'src', 'build')));

const default_active = {
    home: "",
    themes: "",
    contact: "",
    pricing: ""
}

app.get('/', homeController.index);
app.get('/contact', contactController.index);
app.get('/pricing', pricingController.index);
app.get('/themes', themesController.index);
app.get('/themes/:name', themesController.single);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  const page = req.app.get('env') === 'development' ? "error" : "main/contact";

  // render the error page
  res.status(err.status || 404);
  res.render(page, {message: err.message});
});

module.exports = app;
