# captain-design.com
captain-design.com website;

# How to deploy (not recommanded)
- Create build directory and copy in it appspec.yml scripts/ and public/

- Deploy to s3 bucket
```
aws deploy push --application-name CaptainDesign --description "latest update of website" --ignore-hidden-files --s3-location s3://www.captain-design.com/website.zip --source .
```

- Deploy the revision, eTag is generated from deploy push

```
aws deploy create-deployment --application-name CaptainDesign \
--s3-location bucket=www.captain-design.com,key=website.zip,bundleType=zip,eTag=33932c2154e9ec0e80804c2df49adec0-6 \
--deployment-group-name CaptainDesign-prod \
--deployment-config-name CodeDeployDefault.OneAtATime --description "some description"
```

# OR run

npm run build
npm run aws-s3
and next run "aws deploy create-deployment command"

WIP:
- deploy to aws
- title for every theme
- Add title on themes pages
- add sitemap
- subscibe to google webmaster tools


To install nodejs on aws ubuntu
curl -sL https://deb.nodesource.com/setup_9.x | sudo -E bash -
